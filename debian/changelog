libmail-mboxparser-perl (0.55-5) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Tim Retout from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 16:06:53 +0100

libmail-mboxparser-perl (0.55-4.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 00:49:58 +0100

libmail-mboxparser-perl (0.55-4) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Add patch pod-encoding.patch. (Closes: #710798)
  * Update fix-spelling-error-in-manpage.patch. Add another spelling fix.
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Set Standards-Version to 3.9.4 (no changes).
  * Bump debhelper compatibility level to 8.
  * Improve short description.

 -- gregor herrmann <gregoa@debian.org>  Sun, 02 Jun 2013 20:32:00 +0200

libmail-mboxparser-perl (0.55-3) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Tim Retout ]
  * Email change: Tim Retout -> diocles@debian.org

  [ Salvatore Bonaccorso ]
  * Add perl-Mail-MboxParser-0.55-Fix-garbled-attachment-name-RT-66576.patch
    patch to patch the attachment name in t/10_qpnames.t which is not
    valid UTF-8 and thus does not match text stored in the mbox. Fixes FTBFS
    (Closes: #615545).
  * Convert to '3.0 (quilt)' source package format.
  * Remove README.source and drop quilt framework.
  * debian/control:
    - Drop quilt from Build-Depends.
    - Change versioned Build-Depends-Indep on perl (>= 5.8.0-7) to a
      unversioned dependency on perl as the version is already satisfied
      in oldstable.
  * debian/rules: Use tiny rules makefile.
  * debian/copyright:
    - Update to revision 135 of DEP5's format-specification for
      machine-readable copyright file information.
    - Update copyright for debian/* packaging.
    - Explicitly point to GPL-1 license text in common-licenses.
  * Bump Standards-Version to 3.9.2.
  * Add fix-spelling-error-in-manpage.patch patch to fix spelling errors
    in manpage.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 25 Jun 2011 14:19:03 +0200

libmail-mboxparser-perl (0.55-2) unstable; urgency=low

  [ Tim Retout ]
  * Take over for the Debian Perl Group. (Closes: #460181)
  * debian/compat: Bump from 4 to 7.
  * debian/control:
    + Change Maintainer to Debian Perl Group.
    + Add self to Uploaders.
    + Bump Standards-Version to 3.8.0.
    + Add new Homepage and Vcs-* fields.
    + Bump debhelper Build-Depends, and replace dpatch with quilt.
    + Add libtest-pod-perl and libtest-pod-coverage-perl to
      Build-Depends-Indep field.
    + Replace libmime-perl with libmime-tools-perl in Build-Depends-Indep and
      Depends fields.
    + Move liburi-find-perl from Suggests to Recommends.
    + Revise Description.
  * debian/copyright: Rewrite in new format.
  * debian/libmail-mboxparser-perl.examples:
    + New file; ship upstream example scripts.
  * debian/patches/00list: Remove.
  * debian/patches/01_fix_messageparser_interaction.dpatch:
    + Rename to just fix_messageparser_interaction.
    + Quilt refresh, and add a description.
  * debian/patches/series: New file.
  * debian/README.source: New file.
  * debian/rules: Rewrite.
  * debian/watch: New watch file courtesy of dh-make-perl.

  [ gregor herrmann ]
  * Use "$(QUILT_STAMPFN)" instead of "patch" in debian/rules.
  * debian/control: wrap a long line.

 -- Tim Retout <tim@retout.co.uk>  Fri, 15 Aug 2008 22:14:50 -0300

libmail-mboxparser-perl (0.55-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Remove /usr/lib/perl5 only if it exists (needed to avoid FTBFS after
    Perl 5.10 transition) (Closes: #479933)

 -- Gunnar Wolf <gwolf@debian.org>  Wed, 28 May 2008 10:13:49 -0500

libmail-mboxparser-perl (0.55-1) unstable; urgency=low

  * New upstream release.
  * Apply David Coppit's patch to fix hangs during 'make test'.
    closes: #395268
  * Bump to Standards-Version 3.7.2, no changes required.

 -- Joshua Kwan <joshk@triplehelix.org>  Sat, 09 Jun 2007 02:14:26 -0700

libmail-mboxparser-perl (0.54-1) unstable; urgency=low

  * New upstream release.
  * Bump to Standards-Version 3.6.2 - no changes required.

 -- Joshua Kwan <joshk@triplehelix.org>  Wed, 27 Jul 2005 21:25:01 -0700

libmail-mboxparser-perl (0.51-1) unstable; urgency=low

  * Initial release. closes: #290991

 -- Joshua Kwan <joshk@triplehelix.org>  Mon, 17 Jan 2005 20:04:37 -0800
